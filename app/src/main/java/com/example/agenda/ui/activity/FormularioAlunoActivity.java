package com.example.agenda.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.agenda.R;
import com.example.agenda.model.Aluno;
import com.example.agenda.model.AlunoDAO;

public class FormularioAlunoActivity extends AppCompatActivity {

    private EditText campoNome, campoTelefone, campoEmail;
    private Button botaoSalvar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_aluno);

        binding();

        botaoSalvar.setOnClickListener(cliqueSalvar());
    }

    private View.OnClickListener cliqueSalvar() {

        return  new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String nome =  campoNome.getText().toString();
               String telefone = campoTelefone.getText().toString();
               String email = campoEmail.getText().toString();

               Aluno alunoCriado = new Aluno(nome,telefone, email);
               AlunoDAO dao = new AlunoDAO();

                dao.salva(alunoCriado);

               finish();
            }
        };
    }

    private void binding() {
         botaoSalvar = findViewById(R.id.activity_formulario_aluno_botao_salvar);
         campoNome = findViewById(R.id.activity_formulario_aluno_nome);
         campoTelefone = findViewById(R.id.activity_formulario_aluno_Telefone);
         campoEmail = findViewById(R.id.activity_formulario_aluno_email);
    }
}